import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Provider } from "react-redux";
import store from "../../redux/store";
import Search from "./Search";


describe("<Search />", () => {
  test("check event keyboard text field", () => {
    const searchComponent = render(
      <Provider store={store}>
        <Search />
      </Provider>
    );

    const input = searchComponent.getByRole("textbox");
    userEvent.type(input, "React");

    expect(input.value).toEqual("React");
  });
});
