import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import './Loading.css'

export const Loading = () =>  {

  return (
    <div className='loading'>
      <CircularProgress/>
      <span>Loading...</span>
    </div>
  );
}