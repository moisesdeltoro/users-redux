import React from 'react'
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import { Button, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {DELETE_USER_REQUESTED} from '../../redux/actions/user-actions'
import { useDispatch } from 'react-redux';

const useStyles = makeStyles({
    root: {
      minWidth: 275,
      marginTop: 20
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
  });

export const CardUser = ({user}) => {
    const classes = useStyles();
    const dispatch = useDispatch()
    const deleteUser = () => {
        dispatch({type: DELETE_USER_REQUESTED, payload: user.id})
    }
    return (
        <Card className={classes.root}>
        <CardContent>
          <Typography color="textSecondary" gutterBottom>
            {user.name}
          </Typography>
        </CardContent>
        <CardActions>
          <Button size="small" color='primary' onClick={deleteUser}>Delete</Button>
        </CardActions>
      </Card>
    )
}
