import React from "react";
import {
  render,
  screen,
  waitForElementToBeRemoved,
  
} from "@testing-library/react";
import UserEvent from "@testing-library/user-event";
import { Home } from "./Home";

import { Provider } from "react-redux";
import store from "../../redux/store";


describe("Testing Home", () => {
  test("loading text is shown while API request is in progress", async () => {
    const home = render(
      <Provider store={store}>
        <Home />
      </Provider>
    );
    const loading = screen.getByText("Loading...");
    expect(loading).toBeInTheDocument();
    await waitForElementToBeRemoved(() => screen.getByText("Loading..."));
  });

  test("Users not found", () => {

    render(
      <Provider store={store}>
        <Home />
      </Provider>
    );

    const input = screen.getByRole("textbox");
    UserEvent.type(input, "Lorem ip ne");

    const userFound = screen.getByText("Users not found");
    expect(userFound).toBeInTheDocument();
  });

});
