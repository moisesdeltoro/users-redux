import React, { useEffect, useState } from "react";
import { FILTER_USERS, GET_USERS_REQUEST } from "../../redux/actions/user-actions";
import "./Home.css";
import { useSelector, useDispatch } from "react-redux";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import { CardUser } from "../../components/CardUser/CardUser";
import { Loading } from "../../components/Loading/Loading";
import Search from '../../components/Search/Search'

export const Home = () => {
  const usersFiltered = useSelector((state) => state.usersFiltered);

  const loading = useSelector((state) => state.loading);
  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch({ type: GET_USERS_REQUEST });
  }, []);


  

  return (
    <>
      {loading && <Loading />}
      <CssBaseline />
      <Container maxWidth="lg">
        <Search />
        <div className="container-card">
          {usersFiltered.length !== 0 ? (
            usersFiltered.map((user) => {
              return (
                <React.Fragment key={user.id}>
                  <CardUser user={user} />
                </React.Fragment>
              );
            })
          ) : (
            <p>Users not found</p>
          )}
        </div>
      </Container>
    </>
  );
};
