// Import the redux-saga/effects
import {
    put,
    call,
    takeLatest,
    takeEvery
  } from 'redux-saga/effects'
  
  // Import all actions and api's
  import {
    SET_LOADING,
    CREATE_USER,
    CREATE_USER_REQUEST,
    DELETE_USER_REQUESTED,
    DELETE_USER,
    GET_USERS,
    GET_USERS_REQUEST,
    SET_ERROR,
  } from '../actions/user-actions'
  
  // Import all api's
  import {
      createUser,
      deleteUser,
      getAllUsers
  } from '../../api/user-api'
  
  // Here's the unique part, generator function*, function with asterisk(*)
  
  // Get Todos
  function* getUsers() {
    yield put({ type: SET_LOADING })
  
    try {
      const users = yield call(getAllUsers)
      yield put({ type: GET_USERS, payload: users })
      
    } catch (error) {
      yield put({ type: SET_ERROR})
    }
  
  }
  
  // Create Todo
  function* createNewUser({ payload }) {
    yield put({ type: SET_LOADING })
  
    const newUser = yield call(createUser, payload)
  
    yield put({ type: CREATE_USER, payload: newUser })
    
  }
  
  // Delete todo
  function* deleteExistUser({ payload }) {
    yield put({ type: SET_LOADING })
  
    const users = yield call(deleteUser, payload)
  
    yield put({ type: DELETE_USER, payload: users })
  }


  // Export the saga (todo-saga)
  export default function* todoSaga() {
    yield takeEvery(GET_USERS_REQUEST, getUsers)
    yield takeEvery(CREATE_USER_REQUEST, createNewUser)

    yield takeLatest(DELETE_USER_REQUESTED, deleteExistUser)
  }