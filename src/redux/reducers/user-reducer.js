import {
  CREATE_USER,
  DELETE_USER,
  GET_USERS,
  SET_LOADING,
  SET_ERROR,
  FILTER_USERS,
} from "../actions/user-actions";

// Define your state here
const initialState = {
  loading: false,
  users: [],
  usersFiltered: [],
};

// This export default will control your state for your application
export const UserReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    // Set loading
    case SET_LOADING:
      return {
        ...state,
        loading: true,
      };
    // Get todos
    case GET_USERS:
      return {
        ...state,
        users: payload,
        usersFiltered: payload,
        loading: false,
      };
    // Set todo title from user that gonna input a title in form
    // Create new todo
    case CREATE_USER:
      return {
        ...state,
        users: [payload, ...state.users],
        loading: false,
      };
    // Clear todo title in form after creating a new one
    // Delete existed todo
    case DELETE_USER:
      return {
        ...state,
        users: state.users.filter((user) => user.id !== payload),
        usersFiltered: state.usersFiltered.filter((user) => user.id !== payload),

        loading: false,
      };
    case SET_ERROR:
      return {
        ...state,
        users: [],
        loading: false,
        error: true,
      };
    case FILTER_USERS:
      return {
        ...state,
        usersFiltered: payload,
      };
    // Return default state if you didn't match any case
    default:
      return state;
  }
};
