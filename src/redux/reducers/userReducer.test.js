import { UserReducer } from "./user-reducer";

describe("reducer user", () => {
    test('should return the initial state', () => {
        expect(UserReducer(undefined, {})).toEqual(
          {
            loading: false,
            users: [],
            usersFiltered: []
          }
        )
      })
});
