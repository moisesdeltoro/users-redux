import defaultAxios from 'axios'

const axios = defaultAxios.create({
  baseURL: 'https://jsonplaceholder.typicode.com/',
  headers: {'Content-Type': 'application/json'}
});

// Get All Todos
export const getAllUsers = async () => {
	try {
		const todos = await axios.get('users')

		return todos.data
	} catch(err) {
		return new Error("Not found Users")
	}
}

// Create New Todo
export const createUser = async (users) => {
	try {
        let user = users[0]
		const todo = await axios.post('users', {
			user
		})

		return todo.data
	} catch(err) {
		return console.error(err)
	}
}

// Delete existed todo
export const deleteUser = async (id) => {
	try {
		await axios.delete(`users/${id}`)

		return id
	} catch(err) {
		return console.error(err)
	}
}